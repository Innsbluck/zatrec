"use client";

import ScrollToTop from "@/components/ScrollToTop";
import styles from "@/styles/DR3315.module.scss";
import Image from "next/image";

export default function hidden3() {
  return (
    <div>
      <div
        style={{
          width: "100%",
          height: "100vh",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <a href="/confidential/dr3315.jpg" target="_blank">
          <Image
            src="/confidential/dr3315.jpg"
            alt=""
            width={600}
            height={1000}
            className={styles.dr}
          />
        </a>

        <div className={styles.spacer} />

        <p>
          <a href="/home">back</a>
        </p>
      </div>
    </div>
  );
}
