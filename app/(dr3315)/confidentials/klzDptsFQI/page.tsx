"use client";

import ScrollToTop from "@/components/ScrollToTop";
import styles from "@/styles/DR3315.module.scss";
import Image from "next/image";

export default function hidden1() {
  return (
    <div className={styles.root}>
      <ScrollToTop />

      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Image
          src="/confidential/111.jpg"
          alt=""
          width={600}
          height={1000}
          className={styles.image}
        />

        <div className={styles.spacer} />

        <a href="/confidentials/gyHJymugiT">
          <Image
            src="/antenna/antenna_1.png"
            alt=""
            width={1000}
            height={1000}
            style={{  
              width: "15px",
              height: "auto",
            }}
          />
        </a>
      </div>
    </div>
  );
}
