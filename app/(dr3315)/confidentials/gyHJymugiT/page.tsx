"use client";

import ScrollToTop from "@/components/ScrollToTop";
import styles from "@/styles/DR3315.module.scss";
import Image from "next/image";

export default function hidden2() {
  return (
    <div className={styles.root}>
      <ScrollToTop />

      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <a href="/confidentials/eDopOBHtZO">
          <Image
            src="/antenna/antenna_2.png"
            alt=""
            width={1000}
            height={1000}
            style={{
              width: "15px",
              height: "auto",
            }}
          />
        </a>

        <div className={styles.spacer} />

        <Image
          src="/confidential/b.jpg"
          alt=""
          width={600}
          height={1000}
          className={styles.image}
        />
      </div>
    </div>
  );
}
