import type { Metadata } from "next";
import { Inter } from "next/font/google";

import styles from "@/styles/layout.module.scss";
import "@/styles/globals.scss";
import LangtonCanvas from "@/components/LangtonCanvas";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "dr3315",
  description: "dr3315",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html>
      <body className={inter.className}>
        <main style={{justifyContent: "center", alignItems: "center", display: "flex"}}>{children}</main>
      </body>
    </html>
  );
}
