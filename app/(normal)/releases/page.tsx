import Image from "next/image";
import styles from "@/styles/Releases.module.scss";
import ReleasesGrid from "@/components/ReleasesGrid";
import Tab from "@/components/Tab";
import ScrollToTop from "@/components/ScrollToTop";

export default function Releases() {
  return (
    <div className={styles.content}>
      <ScrollToTop />
      {/* <p className={styles.headline}>Releases</p> */}

      <div className={styles.grid_container}>
        <ReleasesGrid />
      </div>
    </div>
  );
}
