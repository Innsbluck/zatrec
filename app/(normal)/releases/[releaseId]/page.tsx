import BackButtonArea from "@/components/BackButtonArea";
import ReleaseDetail from "@/components/ReleaseDetail";
import ScrollToTop from "@/components/ScrollToTop";
import { Releases } from "@/components/data/Releases";
import styles from "@/styles/ReleaseDetailPage.module.scss";
import { useRouter } from "next/router";

export default function ReleaseDetailPage({
  params,
}: {
  params: { releaseId: string };
}) {
  const release =
    Releases.find((e) => e.releaseId === params.releaseId) || null;

  return (
    <div className={styles.content}>
      
      <ScrollToTop />
      <BackButtonArea href={"/releases"} text="back to releases" />

      {release ? (
        <div>
          <ReleaseDetail release={release} />
        </div>
      ) : (
        <div className={styles.not_found}>
          <p className={styles.not_found_icon}>Went Wrong?</p>
          <p className={styles.not_found_text}>
            "{params.releaseId}" is not a valid release...
            <br />
            try again with another id.
          </p>
        </div>
      )}
    </div>
  );
}
