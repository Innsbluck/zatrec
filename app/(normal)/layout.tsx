import Header from "@/components/Header";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import Image from "next/image";

import styles from "@/styles/layout.module.scss";
import "@/styles/globals.scss";
import Footer from "@/components/Footer";
import LangtonCanvas from "@/components/LangtonCanvas";
import SPMenuArea from "@/components/SPMenuArea";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "zattō records",
  description: "zattō records",
  icons: {
    icon: "/favicon.ico",
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html>
      <body className={inter.className}>
        <div className={styles.center}>
        </div>

        <Header />
        <Footer />
        <div className={styles.canvas_container}>
          <LangtonCanvas />
        </div>

        <div className={styles.bg_dot}>
          <div className={styles.bg_main}></div>
        </div>

        <main className={styles.main}>{children}</main>


        <SPMenuArea />
      </body>
    </html>
  );
}
