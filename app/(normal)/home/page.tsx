
import { FlavorText } from "@/components/FlavorText";
import ScrollToTop from "@/components/ScrollToTop";
import styles from "@/styles/Home.module.scss";

export default function Home() {
  return (
    <div className={styles.root}>
      <ScrollToTop />
      <FlavorText />
      <p className={styles.flavor_sub}>We are zattō records.</p>

    </div>
  );
}
