import styles from "@/styles/CharacterDetail.module.scss";
import { Profiles } from "@/components/data/Profiles";
import BackButtonArea from "@/components/BackButtonArea";
import ScrollToTop from "@/components/ScrollToTop";
import Link from "next/link";
import Image from "next/image";

export default function CharacterDetail({
  params,
}: {
  params: { charaId: string };
}) {
  let profile = Profiles.find((e) => e.id === params.charaId);

  if (!profile) return <p>What?</p>;

  let index = Profiles.indexOf(profile);

  let prevProf, nextProf;
  if (index > 0) prevProf = Profiles[index - 1];
  if (index < Profiles.length - 1) nextProf = Profiles[index + 1];

  return (
    <div className={styles.root}>
      <ScrollToTop />
      <BackButtonArea href={"/characters"} text={"back"} />

      <div className={styles.content}>
        <div className={styles.tatie_container}>
          <Image
            className={styles.tatie_img}
            src={profile.tatie}
            alt={profile.name || ""}
            width={0}
            height={0}
            sizes='100vw'
            quality={50}
          />
          <div className={styles.divider}></div>
        </div>

        <div className={styles.text_area}>
          <div className={styles.antenna_container}>
            <p className={styles.antenna_text}>ANTENNA ─</p>
            <Link
              href={profile.releaseLink || ""}
              style={{ objectFit: "contain" }}
            >
              <Image
                className={styles.antenna}
                src={profile.antenna}
                alt={"antenna"}
                width={50}
                height={50}
              />
            </Link>
          </div>

          <p className={styles.no}>{`no.${profile.no}`}</p>
          <p className={styles.name}>{profile.name}</p>
          <span
            className={styles.details}
            dangerouslySetInnerHTML={{ __html: profile.details || "" }}
          />

          <div className={styles.other_profs}>
            <div className={styles.other_profs_row}>
              <p className={styles.other_profs_title}>身長:</p>
              <p>{`${profile.height}cm`}</p>
            </div>

            <div className={styles.other_profs_row}>
              <p className={styles.other_profs_title}>好きなモノ:</p>
              <ul
                className={styles.other_profs_list}
                style={{ listStyle: "none" }}
              >
                {profile.likes?.map((like, i) => {
                  return (
                    <li key={i}>
                      <p>{`${like}`}</p>
                    </li>
                  );
                })}
              </ul>
            </div>

            <div className={styles.other_profs_row}>
              <p className={styles.other_profs_title}>苦手なモノ:</p>
              <ul
                className={styles.other_profs_list}
                style={{ listStyle: "none" }}
              >
                {profile.dislikes?.map((dislike, i) => {
                  return (
                    <li key={i}>
                      <p>{`${dislike}`}</p>
                    </li>
                  );
                })}
              </ul>
            </div>

            <p className={styles.others}>{profile.others}</p>

            <a
              href={profile.fullImage}
              target="_blank"
              style={{ textDecoration: "underline", marginTop: "1rem" }}
            >
              ■ View Full Image
            </a>
          </div>
        </div>
      </div>
      <div className={styles.next_prev_container}>
        <div className={styles.prev_area}>
          {prevProf && (
            <a href={`/characters/${prevProf.id}`}>
              {"<< "}
              {prevProf.name}
            </a>
          )}
        </div>
        <div className={styles.next_area}>
          {nextProf && (
            <a href={`/characters/${nextProf.id}`}>
              {nextProf.name}
              {" >>"}
            </a>
          )}
        </div>
      </div>
    </div>
  );
}
