import ScrollToTop from "@/components/ScrollToTop";
import styles from "@/styles/Characters.module.scss";
import Image from "next/image";
import Link from "next/link";
import anopla from "@/public/tatie/anopla.webp";
import Hypre from "@//public/tatie/Hypre.webp";
import alesc from "@/public/tatie/alesc.webp";
import Jitlon from "@//public/tatie/Jitlon.webp";

export default function Characters() {
  return (
    <div className={styles.root}>
      <ScrollToTop />
      <p className={styles.float_text}>Click to See Profiles</p>

      <div className={styles.tatie_container}>
        <div className={styles.divider}></div>
        <Link className={styles.tatie_link} href={"/characters/anopla"}>
          <Image
            className={styles.tatie_img}
            src={anopla}
            alt="anopla"
            width={0}
            height={0}
            sizes='100vw'
          />
        </Link>
        <Link className={styles.tatie_link} href={"/characters/hypre"}>
          <Image
            className={styles.tatie_img}
            src={Hypre}
            alt="Hypre"
            width={0}
            height={0}
            sizes='100vw'
          />
        </Link>
        <Link className={styles.tatie_link} href={"/characters/alesc"}>
          <Image
            className={styles.tatie_img}
            src={alesc}
            alt="alesc"
            width={0}
            height={0}
            sizes='100vw'
          />
        </Link>
        <Link className={styles.tatie_link} href={"/characters/jitlon"}>
          <Image
            className={styles.tatie_img}
            src={Jitlon}
            alt="Jitlon"
            width={0}
            height={0}
            sizes='100vw'
          />
        </Link>

        {/* <div>
          <img className={styles.tatie_all} src="/tatie/characters.svg" />

          <div className={styles.link}></div>
        </div> */}
      </div>
    </div>
  );
}
