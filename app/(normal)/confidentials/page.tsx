"use client";

import ScrollToTop from "@/components/ScrollToTop";
import notFoundStyles from "@/styles/404.module.scss";
import Image from "next/image";
import { useState } from "react";

const ASERI = "/anopla aseri trim.webp"
const CHOUASERI = "/anopla chouaseri trim.webp"

export default function DR3315() {
  const [passcode, setPasscode] = useState("");
  const [opened, setOpen] = useState(false);

  return (
    <div className={notFoundStyles.root}>
      <ScrollToTop />

      <div className={notFoundStyles.container}>
        <Image
          className={notFoundStyles.anopla}
          width={800}
          height={800}
          quality={50}
          alt={"asease"}
          src={opened ? ASERI: CHOUASERI}
        />
        <div>
          <p className={notFoundStyles.oops}>Oops!</p>
          <p className={notFoundStyles.oops_sub}>なにもなかったみたいです...</p>
          <form
            className={notFoundStyles.form}
            onSubmit={(e) => {
              if (passcode === "relationship") {
                e.preventDefault();
                setOpen(true);
              }
            }}
          >
            <a href={"/home"}>ホームに戻る</a>　　　
            <input
              className={notFoundStyles.input}
              type="text"
              placeholder="Enter passphrase..."
              onInput={(e) => setPasscode((e.target as HTMLInputElement).value)}
              style={{ opacity: 0.2 }}
            />
          </form>

          <button
            style={{
              visibility: opened ? "visible" : "collapse",
              background: "white",
              margin: 30,
            }}
            onClick={() => {
              window.location.replace("/confidentials/klzDptsFQI");
            }}
          >
            <p>ENTER</p>
          </button>
        </div>
      </div>
    </div>
  );
}
