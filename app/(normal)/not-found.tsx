import ScrollToTop from "@/components/ScrollToTop";
import styles from "@/styles/404.module.scss";
import Image from "next/image";

export default function NotFound() {
  return (
    <div className={styles.root}>
      <ScrollToTop />

      <div className={styles.container}>
        <Image
          className={styles.anopla}
          width={800}
          height={800}
          quality={50}
          alt={"asease"}
          src={"/anopla chouaseri trim.webp"}
        />
        <div>
          <p className={styles.oops}>Oops!</p>
          <p className={styles.oops_sub}>なにもなかったみたいです...</p>
          <a href={"/home"}>ホームに戻る</a>
        </div>
      </div>
    </div>
  );
}
