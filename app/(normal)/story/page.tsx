"use client";

import ScrollToTop from "@/components/ScrollToTop";
import styles from "@/styles/Story.module.scss";
import { useEffect, useState } from "react";

const DATA_TEXT = `Restored Data: "Report #31"

Original Date: [unknown]
Restored Date: ■■■■/■/■■

[Restoration Starting...]`;

const CONTENT_TEXT = `　太陽系の形成は約46億年前、巨大な分子雲の収縮に始まり、分子雲の大部分は太陽を形成、それ以外がその他の惑星や衛星を形作ったとされる。


　（中略）


　──そのような惑星の中でも、土星は特に巨大な環を持つことで知られる。環の内容は数μm〜数mの無数の粒子だとされているが、その中には意識─所有者のいない意識そのもの─の断片が数多く存在していた。


　それらの意識の断片は目に見える実体を持たないことから長らく存在が知られずにいたが、2020年1月8日に突如として起こった最初の「還流」(reflux)[注1]によってそれらのごく一部が凝固し、一つの個体を形成するに至った。その発生が土星ではなく地球上で起こった理由は不明である。




　その個体(以降、『anopla』とする)は実体こそ持たないが、通常の人間とほとんど変わらない姿と自我を備えている。容姿において人間と唯一異なる点として、anoplaの頭部には◯と∀によって構成された記号が浮遊していた。anopla以降発生した個体にもこれに類似した記号が付随しており、後述の機能的特徴からこれらの記号は「アンテナ」と呼ばれるようになった。


　「アンテナ」は土星環の意識断片群に何等かの手段で接続されており、個体と土星環意識群が同調[注2]する際に、anoplaの発生と同様、意識断片の凝固から新しい個体が生み出されるとみられる。これはanopla以降では2021年4月20日の『Hypre』の発生が初めてであり………


………


………各個体の発生の観測については、anopla, Hypreが発生した際の■■■■■■、■■■■■■■における強烈な閃光の様子(Fig ■.4、Fig ■.5)を見ればわかる通り、発生の観測自体は比較的容易である。このようなケース以外の兆候の観測については、センシング部隊"garbage_collecting"が引き続き調査を行い………


………


[Restoration Finished.]




【注釈】
[1] 最初の「還流」の発生要因については諸説存在するが、何者か(地球人であるとされる)の意識が土星方面へと飛ばされ、それに伴い宿主を求める意識の断片の一部が引き寄せられる形で集合したことが原因だとする説が現在では最も有力である。
　ただし、もともと土星に飛来してきた何者かの意識は地点:epi9000において一切の痕跡が消失しており、現在の所在も不明である。

[2] 現在の研究では、同調の発生は各個体の心理状態が関係しているとされる。個体の情動による新たな個体の形成を一種の進化だとする主張もあるが、個体間の特徴に違いはあっても著しい機能の向上などは見られないため、単に分裂や増殖のようなものであるとする見方が一般的である。`;

export default function Story() {
  const [dataTextToShow, setDataTextToShow] = useState("");
  const [contentTextShown, setContentTextShown] = useState(false);

  useEffect(() => {
    let nowCount = 0;
    let glitchCount = 7;

    const interval = setInterval(() => {
      nowCount++;
      let next = DATA_TEXT.slice(0, nowCount);

      let glitchTextLength = Math.min(glitchCount, DATA_TEXT.length - nowCount);

      for (let i = 0; i < glitchTextLength; i++) {
        let randomIndex = Math.floor(Math.random() * DATA_TEXT.length);
        let randomChar = DATA_TEXT.charAt(randomIndex);
        if (randomChar === "\n") randomChar = "▓";
        next += randomChar;
      }

      setDataTextToShow(next);

      if (nowCount >= DATA_TEXT.length) {
        clearInterval(interval);

        setContentTextShown(true);
      }
    }, 5);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <div className={styles.content}>
      <ScrollToTop />
      <div className={styles.text_container}>
        <p className={styles.data_text}>{dataTextToShow}</p>
        <p className={contentTextShown ? styles.text_active : styles.text}>
          {CONTENT_TEXT}
        </p>
      </div>
    </div>
  );
}
