"use client";

import Link from "next/link";
import { TabContent } from "./Tab";

import styles from "@/styles/TabItem.module.scss";

const TabItem = ({ tabContent }: { tabContent: TabContent }) => {
  if (tabContent.title === "confidentials") {
    return (
      <div className={styles.root}>
        <div className={styles.reveal_container}>
          <div className={styles.overlay} />
          <Link
            href={tabContent.url}
            className={styles.confidentials}
            prefetch={false}
          >
            {tabContent.title}
          </Link>
        </div>

        {tabContent.isActive ? <div className={styles.bar} /> : <></>}
      </div>
    );
  } else {
    return (
      <div className={styles.root}>
        <Link
          href={tabContent.url}
          className={tabContent.isActive ? styles.title_active : styles.title}
          prefetch={false}
        >
          {tabContent.title}
        </Link>

        {tabContent.isActive ? <div className={styles.bar} /> : <></>}
      </div>
    );
  }
};

export default TabItem;
