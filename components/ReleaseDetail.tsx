"use client";

import { ReleaseItem } from "./data/Releases";
import { motion } from "framer-motion";
import Link from "next/link";
import styles from "@/styles/ReleaseDetail.module.scss";

const ReleaseDetail = ({ release }: { release: ReleaseItem }) => {
  const year = release.releaseDate.getFullYear();
  const month = release.releaseDate.getMonth();
  const day = release.releaseDate.getDay();
  const releaseDate = `${year}/${month}/${day}`;

  return (
    <motion.div
      initial={{ opacity: 0 }} // 初期状態
      animate={{ opacity: 1 }} // マウント時
      exit={{ opacity: 0 }} // アンマウント時
    >
      <div className={styles.root}>
        <img className={styles.cover_img} src={release.coverUrl} />
        <div className={styles.text_area}>
          <p className={styles.detail}>
            {release.artist + "/releases/" + release.releaseDate.getFullYear()}
          </p>
          <p className={styles.title}>{release.title}</p>
          {release.songs.map((title, index) => {
            return <p className={styles.track}>{`${index + 1}. ${title}`}</p>;
          })}

          <p className={styles.listen_in}>available in:</p>
          <Link
            className={styles.available_in}
            href={`https://zatrec.bandcamp.com/album/${release.releaseId}`}
          >
            <p className={styles.available_in_icon}>▶</p>
            <p className={styles.available_in_text}>bandcamp</p>
          </Link>

          <p className={styles.release_date}>Release: {releaseDate}</p>
        </div>

        {/* <iframe
        style={{ border: 0, width: 350, height: 470, zIndex: 2 }}
        src={`https://bandcamp.com/EmbeddedPlayer/album=${release.bcId}/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/transparent=true/`}
        seamless
      >
      </iframe> */}
      </div>
    </motion.div>
  );
};

export default ReleaseDetail;
