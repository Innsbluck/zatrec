"use client";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBandcamp,
  faGitlab,
  faSoundcloud,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import styles from "@/styles/LinkButtons.module.scss";
import Link from "next/link";

export default function LinkButtons({ isDark }: { isDark: boolean }) {
  return (
    <div className={styles.link_container}>
      <Link className={styles.icon_link} href="https://twitter.com/alphendp">
        <FontAwesomeIcon className={isDark ? styles.icon_tw_dark : styles.icon_tw_light } icon={faTwitter} />
      </Link>
      <Link
        className={styles.icon_link}
        href="https://zatrec.bandcamp.com/music"
      >
        <FontAwesomeIcon className={isDark ? styles.icon_bc_dark : styles.icon_bc_light } icon={faBandcamp} />
      </Link>
      <Link
        className={styles.icon_link}
        href="https://soundcloud.com/alph-endp"
      >
        <FontAwesomeIcon className={isDark ? styles.icon_sc_dark : styles.icon_sc_light } icon={faSoundcloud} />
      </Link>
      <Link
        className={styles.icon_link}
        href="https://gitlab.com/Innsbluck/zatrec"
      >
        <FontAwesomeIcon className={isDark ? styles.icon_gl_dark : styles.icon_gl_light } icon={faGitlab} />
      </Link>
    </div>
  );
}
