import { title } from "process";
import { ReactNode } from "react";

export interface ReleaseItem {
  releaseId: string,
  bcId: number,
  releaseDate: Date;
  type: string;
  artist: string;
  title: string;
  coverUrl: string;
  songs: string[];
  bandcampUrl: string;
}

export const Releases: ReleaseItem[] = [
  {
    releaseDate: new Date(2019, 11, 18),
    type: "album",
    artist: "garbage_collecting",
    title: "Record of Disorder",
    releaseId: "record-of-disorder",
    bcId: 3985587989,
    coverUrl: "https://f4.bcbits.com/img/a3511866450_10.jpg",
    songs: [
      "check...",
      "gtlp",
      "restore",
      "koukasita",
      "village",
      "minus",
      "yoake",
      "huge heritage",
      "heartstroke",
      "なんとなく(atmospheric dub mix)",
      "N",
      "逃避行",
    ],
    bandcampUrl: "https://zatrec.bandcamp.com/album/record-of-disorder",
  },
  {
    releaseDate: new Date(2019, 12, 4),
    type: "ep",
    artist: "garbage_collecting",
    title: "Forgot Technologies",
    releaseId: "forgot-technologies",
    bcId: 788976526,
    coverUrl: "https://f4.bcbits.com/img/a4115623827_10.jpg",
    songs: ["wreckship", "マザーシップを守りきれ！！"],
    bandcampUrl: "https://zatrec.bandcamp.com/album/forgot-technologies",
  },
  {
    releaseDate: new Date(2020, 1, 8),
    type: "album",
    artist: "alphendp",
    title: "another planet",
    releaseId: "another-planet",
    bcId: 2306390710,
    coverUrl: "https://f4.bcbits.com/img/a3027405824_10.jpg",
    songs: [
      "zatto vs riot",
      "reflux",
      "hello cheap casio",
      "linear top speed",
      "untitled",
      "epi 9000",
      "alarm still sounds",
      "return",
    ],
    bandcampUrl: "https://zatrec.bandcamp.com/album/another-planet",
  },
  {
    releaseDate: new Date(2021, 4, 20),
    type: "album",
    artist: "alphendp",
    title: "Hypersignal Reports",
    releaseId: "hypersignal-reports",
    bcId: 312394903,
    coverUrl: "https://f4.bcbits.com/img/a2273964507_10.jpg",
    songs: [
      "DRY Shift",
      "Battles against PCM!!",
      "L4 STEREO-A",
      "Slip-on Toxic Blast",
      "Laster",
      "Descend|Discend",
    ],
    bandcampUrl: "https://zatrec.bandcamp.com/album/hypersignal-reports",
  },
  {
    releaseDate: new Date(2021, 7, 7),
    type: "ep",
    artist: "garbage_collecting",
    title: "Tight-budget Interspace Warp",
    releaseId: "tight-budget-interspace-warp",
    bcId: 859232906,
    coverUrl: "https://f4.bcbits.com/img/a2762732858_10.jpg",
    songs: ["discharge", "injection", "retrograde", "escape velocity"],
    bandcampUrl:
      "https://zatrec.bandcamp.com/album/tight-budget-interspace-warp",
  },
  {
    releaseDate: new Date(2022, 12, 23),
    type: "ep",
    artist: "alphendp",
    title: "alert scud direct",
    releaseId: "alert-scud-direct",
    bcId: 715405324,
    coverUrl: "https://f4.bcbits.com/img/a4120194075_10.jpg",
    songs: [
      "preheat dielectric",
      "dialectal entire scud",
      "duplex [epi 3700]",
      "end of …..",
    ],
    bandcampUrl: "https://zatrec.bandcamp.com/album/alert-scud-direct",
  },
  {
    releaseDate: new Date(2022, 12, 23),
    type: "album",
    artist: "alphendp",
    title: "Jitter Longshot",
    releaseId: "jitter-longshot",
    bcId: 1703343544,
    coverUrl: "https://f4.bcbits.com/img/a1999358055_10.jpg",
    songs: [
      "FDTRDD",
      "Depth'n pm",
      "nRect",
      "cert/spin",
      "stepin' admtt spikes",
      "Leap"
    ],
    bandcampUrl: "https://zatrec.bandcamp.com/album/jitter-longshot",
  },
];

export function findReleaseById(id: string) {
  return Releases.find((e: ReleaseItem) => e.releaseId === id)
}

function getAlbumTitleToID(title:string) {
  return title.toLowerCase().replace(/ /g, '-');
}