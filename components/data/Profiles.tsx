import { findReleaseById } from "./Releases";

export interface Profile {
  id: string;

  no: string;
  tatie: string;
  fullImage: string;
  antenna: string;
  releaseLink?: string;

  name?: string;
  height?: Number;

  details?: string;

  likes?: string[];
  dislikes?: string[];

  others?: string;
}

export const Profiles: Profile[] = [
  {
    id: "anopla",
    no: "01",
    tatie: "/tatie/anopla_up.png",
    fullImage: "/tatie/anopla.png",
    antenna: "/antenna/antenna_1.png",
    releaseLink: findReleaseById('another-planet')?.bandcampUrl,
    name: "anopla",
    height: 155,
    details: `「還流」によって生み出された最初の個体。<br><br>
      フレンドリーな性格。後先のことを考えない行動が多い。<br>
      Hypreの誕生直後に殺されかけた経験があり、Hypreの懐柔役になったalescに感謝している。<br>
      暑さに弱く寒さに弱い。`,
    likes: ["F-94WA-9JF (Casio)", "猫"],
    dislikes: ["早寝早起き"],
  },
  {
    id: "hypre",
    no: "02",
    tatie: "/tatie/Hypre_up.png",
    fullImage: "/tatie/Hypre.png",
    antenna: "/antenna/antenna_2.png",
    releaseLink: findReleaseById('hypersignal-reports')?.bandcampUrl,
    name: "Hypre",
    height: 163,
    details: `2番目の個体。<br><br>
    凶暴そうな風貌をしている。凶暴である。<br>
    数本の赤く長い髪(?)は本人の感情に合わせて増えたり減ったり動いたりするが、本人としては使い道がなく邪魔なだけらしい。<br>
    度々起こす暴走を止めてくるalescを敵視している。<br>
    
    スイッチヒッター。`,
    likes: ["Data Corrupter (Earthquaker Devices)", "猫"],
    dislikes: ["パズル*", "暖かいもの"],
    others:
      "* 誕生初日に「テスト」と称してUnderstandをプレイさせてきたanoplaを半殺しにしている。",
  },
  {
    id: "alesc",
    no: "03",
    tatie: "/tatie/alesc_up.png",
    fullImage: "/tatie/alesc.png",
    antenna: "/antenna/antenna_3.png",
    releaseLink: findReleaseById('alert-scud-direct')?.bandcampUrl,
    name: "alesc",
    height: 177,
    details: `3番目の個体。<br><br>
    本人曰く、誕生して以降一度も目を開けたことがない。ただし開かないわけではない、とも。<br>
    Hypreの暴走を度々止めている。そのため常に敵意を向けられており、和解の方法に苦心している。<br>
    寝付きと寝起きがとにかく良い。好きな飲料メーカーは大塚製薬。`,
    likes: ["Freeze(Electro-Harmonix)", "猫"],
    dislikes: ["不和"],
  },
  {
    id: "jitlon",
    no: "04",
    tatie: "/tatie/Jitlon_up.png",
    fullImage: "/tatie/Jitlon.png",
    antenna: "/antenna/antenna_4.png",
    releaseLink: findReleaseById('jitter-longshot')?.bandcampUrl,
    name: "Jitlon",
    height: 138,
    details: `4番目の個体。<br><br>
    ▣(未起動時は回)で表されるアンテナは頭部にあるものの他に複数生成する事ができ、瞑想状態では離れた地点にアンテナを設定し、様々な位置から土星環意識群との交信を行うことが可能である。<br>
    ただしアンテナ自体の感度に難があり、雨や雲などがあるとほぼ役に立たなくなってしまう。最近気象予報士資格の取得を真剣に考え始めた。<br>
    好きなバトルチップはダークインビジ。`,
    likes: ["たべっこ水族館(ギンビス)", "猫"],
    dislikes: ["長袖"],
  },
];
