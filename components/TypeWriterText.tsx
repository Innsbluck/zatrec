"use client";

import { useEffect, useState } from "react";

const TypeWriterText = ({
  className,
  intervalMs,
  children,
  ...props
}: {
  className: string;
  intervalMs: number;
  children: string;
}) => {
  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));
  const [text, setText] = useState("");

  useEffect(() => {
    const showTexts = [];
    for (let i = 0; i < children.length; i++) {
      showTexts.push(children.slice(0, i + 1));
    }

    showTexts.forEach((element, index) => {
      setTimeout(() => {
        setText(element)
      }, intervalMs*index);
    });
  }, [children, intervalMs]);

  return <p className={className}>{text}</p>;
};

export default TypeWriterText;
