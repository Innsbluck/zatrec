"use client";

import Link from "next/link";
import styles from "@/styles/BackButtonArea.module.scss";

const BackButtonArea = ({ href, text }: { href: string; text: string }) => {
  return (
    <div className={styles.root}>
      <Link className={styles.link} href={href}>
        <p className={styles.back_text}>{`<  ${text}`}</p>
      </Link>
    </div>
  );
};

export default BackButtonArea;
