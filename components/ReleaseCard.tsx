"use client";

import Link from "next/link";
import { ReleaseItem } from "./data/Releases";
import styles from "@/styles/ReleaseCard.module.scss";

const ReleaseCard = ({ release }: { release: ReleaseItem }) => {
  return (
    <div className={styles.root}>
      {/* <Link className={styles.cover_link} href={`/releases/${release.releaseId}`}> */}
      <Link className={styles.cover_link} href={release.bandcampUrl} target='_blank'> 
        <img className={styles.cover_img} src={release.coverUrl}/>
      </Link>

      <div lang='en' className={styles.text_area}>
        <p className={styles.artist}>{release.artist}</p>
        <p className={styles.title}>{release.title}</p>
      </div>
    </div>
  );
};

export default ReleaseCard;
