"use client";

import styles from "@/styles/Home.module.scss";
import React from "react";

export const FlavorText = () => {
  let flavorText = getFlavorText();

  return (
    <p className={styles.flavor} suppressHydrationWarning={true}>
      {flavorText}
    </p>
  );
};

const flavorTexts = [
  "Enter your own epi.",
  "Reflux and Redux.",
  "Stay cheap wear casio.",
  "That trek was hard.",
  "Per aspera Ad astra.",
  "Not a netlabel.",
  "[whoop whoop] Pull Up.",
  "Nothing lasts but dusts.",
];

function getFlavorText() {
  var randomIndex = Math.floor(Math.random() * flavorTexts.length);
  return flavorTexts[randomIndex];
}
