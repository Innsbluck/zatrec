"use client";

import styles from "@/styles/ReleasesGrid.module.scss";
import { motion } from "framer-motion";
import { ReleaseItem, Releases } from "./data/Releases";
import ReleaseCard from "./ReleaseCard";
import React, { useState } from "react";

enum SORT {
  NEWTOOLD,
  OLDTONEW,
}

const ReleasesGrid = () => {
  const releaseRev = [...Releases].reverse()
  const [releaseSorted, setReleaseSorted] = useState(releaseRev)

  return (
    <motion.div
      initial={{ opacity: 0 }} // 初期状態
      animate={{ opacity: 1 }} // マウント時
      exit={{ opacity: 0 }} // アンマウント時
    >
      <div className={styles.grid}>
        {releaseSorted.map((release: ReleaseItem, index, array) => {
          return <ReleaseCard key={release.bcId} release={release} />;
        })}
      </div>
    </motion.div>
  );
};

export default ReleasesGrid;
