"use client";

import Image from "next/image";
import { useState } from "react";

import styles from "@/styles/SPMenuArea.module.scss";
import SPMenu from "./SPMenu";

const SPMenuArea = ({}) => {
  const [isSPMenuShown, setIsSPMenuShown] = useState(false);

  const onClickSPMenu = () => {
    setIsSPMenuShown(!isSPMenuShown);
  };

  const onCloseSPMenu = () => {
    setIsSPMenuShown(false);
  };

  if (isSPMenuShown) {
    return (
      <div className={styles.menu_container}>
        <SPMenu onClose={onCloseSPMenu} />
      </div>
    );
  } else {
    return (
      <div className={styles.sp_menu_area}>
        <div className={styles.sp_menu_button} onClick={onClickSPMenu}>
          <Image
            className={styles.menu_icon}
            src={"/arrow_drop_up_black_24dp.svg"}
            alt="menu"
            width={24}
            height={24}
            style={{ objectFit: "contain" }}
          ></Image>
          <p className={styles.menu_text}>Menu</p>
        </div>
      </div>
    );
  }
};

export default SPMenuArea;
