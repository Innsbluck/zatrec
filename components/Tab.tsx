"use client";

import { usePathname, useRouter } from "next/navigation";
import TabItem from "./TabItem";

import styles from "@/styles/Tab.module.scss";

export interface TabContent {
  title: string;
  url: string;
  isActive: boolean,
}

export const tabs: TabContent[] = [
  {
    title: "home",
    url: "/home",
    isActive: false,
  },
  {
    title: "releases",
    url: "/releases",
    isActive: false,
  },
  {
    title: "story",
    url: "/story",
    isActive: false,
  },
  {
    title: "characters",
    url: "/characters",
    isActive: false,
  },
  {
    title: "confidentials",
    url: "/confidentials",
    isActive: false,
  },
];

const Tab = ({}) => {
  tabs.forEach(element => {
    element.isActive = false
  });

  const pathname = usePathname()
  const currentPath = "/" +pathname.split("/")[1]
  const currentTab = tabs.find((value) => value.url == currentPath)
  if(currentTab) currentTab.isActive = true

  return (
    <div className={styles.tab_container}>
      {tabs.map((tabContent, index) => {
        return <TabItem tabContent={tabContent} key={index}/>;
      })}
    </div>
  );
};

export default Tab;
