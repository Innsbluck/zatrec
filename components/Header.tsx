"use client";

import React from "react";

import styles from "@/styles/Header.module.scss";
import Link from "next/link";
import Image from "next/image";
import Tab from "./Tab";
import LinkButtons from "./LinkButtons";

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.content}>
        <Link className={styles.logo_link} href="/home" replace>
          <Image
            className={styles.logo}
            src="/logo_for_website.png"
            alt="home"
            width={600}
            height={200}
            style={{ objectFit: "contain" }}
          />
        </Link>

        <div className={styles.tab_container}>
          <Tab />
        </div>

        <div className={styles.link_buttons_container}>
          <LinkButtons isDark={true}/>
        </div>
      </div>
    </header>
  );
};

// const flavorTexts = [
//   "Enter your own epi.",
//   "Reflux and Redux.",
//   "Stay cheap wear casio.",
// ];

// function getFlavorText() {
//   var randomIndex = Math.floor(Math.random() * flavorTexts.length);
//   return flavorTexts[randomIndex];
// }

export default React.memo(Header);
