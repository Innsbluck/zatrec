"use client";

import { faRotateRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { CanvasRenderer, Environment, Terrain, Vector } from "flocc";
import styles from "@/styles/LangtonCanvas.module.scss";
import { useEffect } from "react";

var windowWidth = 0;
var windowHeight = 0;

const LangtonCanvas = () => {
  useEffect(() => {
    function updateSize() {
      windowWidth = window.innerWidth;
      windowHeight = window.innerHeight;
      drawLangton(window.innerWidth, window.innerHeight);
    }

    window.addEventListener("resize", updateSize);

    updateSize();
  }, []);

  function onClickCanvas(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    let randDir = DIRS[Math.floor(Math.random() * 4)];
    let randNTUH = Math.random() < 0.5;
    let type = Math.random();
    let randa = Math.floor(Math.random() * 255);
    let on = { r: 0, g: 0, b: 0, a: 0 };
    if ( type < 0.1) {
      on = { r: 77, g: 141, b: 255, a: randa };
    } else if (0.1 <= type && type < 0.3) {
      on = { r: 0, g: 0, b: 0, a: 0 };
    } else {
      on = { r: 0, g: 0, b: 0, a: randa };
    }
    addAnt({
      x: event.clientX / scale,
      y: event.clientY / scale,
      dir: randDir,
      noTurnUntilHit: randNTUH,
      onColor: on,
    });
  }

  function resetCanvas() {
    ants = [];
    drawLangton(windowWidth, windowHeight);
  }

  return (
    <>
      <div
        style={{
          width: "100vw",
          height: "100vh",
          //cursor: "pointer",
          pointerEvents: "all",
        }}
        onClick={(e) => onClickCanvas(e)}
      >
        <div style={{ opacity: 1, zIndex: 0 }} id="container"></div>
      </div>
      <button className={styles.reset_container} onClick={() => resetCanvas()}>
        <FontAwesomeIcon className={styles.reset} icon={faRotateRight} />
      </button>
    </>
  );
};

export default LangtonCanvas;

const scale = 1;

const NORTH = new Vector(0, -1);
const SOUTH = new Vector(0, 1);
const EAST = new Vector(1, 0);
const WEST = new Vector(-1, 0);

const DIRS = [NORTH, EAST, SOUTH, WEST];

var environment: Environment;
var terrain: Terrain;
var renderer: CanvasRenderer;

const off = { r: 0, g: 0, b: 0, a: 0 };

interface Ant {
  pos: Vector;
  dir: Vector;
  noTurnUntilHit: boolean;
  onColor: { r: number; g: number; b: number; a: number };
}

function addAnt({
  x,
  y,
  dir,
  noTurnUntilHit,
  onColor,
}: {
  x: number;
  y: number;
  dir: Vector;
  noTurnUntilHit: boolean;
  onColor: { r: number; g: number; b: number; a: number };
}) {
  let ant: Ant = {
    pos: new Vector(x, y),
    dir,
    noTurnUntilHit,
    onColor,
  };
  ants.push(ant);
}

var ants: Ant[] = [];

function drawLangton(width: number, height: number) {
  while (width % scale !== 0) width++;
  while (height % scale !== 0) height++;

  environment = new Environment({ width, height });
  terrain = new Terrain(width / scale, height / scale, {
    scale,
  });
  environment.use(terrain);

  renderer = new CanvasRenderer(environment, { width, height });
  renderer.mount("#container");

  //addAnt({
  //  x: (window.innerWidth / (2 * scale)) | 0,
  //  y: (window.innerHeight / (2 * scale)) | 0,
  //  dir: WEST,
  //});

  function setup() {
    terrain.init((x: number, y: number) => off);
  }

  let t = 0;

  function run() {
    ants.forEach((ant, index, ants) => {
      const { pos, dir, noTurnUntilHit } = ant;

      let pixel = terrain.sample(pos.x, pos.y);
      if (typeof pixel == "object")
        terrain.set(pos.x, pos.y, pixel.a == off.a ? ant.onColor : off);
      // update the color where the ant is

      // move the ant
      pos.x += dir.x;
      pos.y += dir.y;

      // turn the ant
      const i = DIRS.indexOf(dir);
      let afterPixel = terrain.sample(pos.x, pos.y);
      if (typeof afterPixel == "object") {
        if (!noTurnUntilHit || afterPixel.a > ant.onColor.a) {
          if (afterPixel.a == off.a) {
            ant.dir = i === DIRS.length - 1 ? DIRS[0] : DIRS[i + 1];
          } else {
            ant.dir = i === 0 ? DIRS[DIRS.length - 1] : DIRS[i - 1];
          }
        }
      }
    });
    if (t < 50) {
      t++;
      return run();
    } else {
      t = 0;
    }
    environment.tick();
    requestAnimationFrame(run);
  }

  setup();
  run();
}
