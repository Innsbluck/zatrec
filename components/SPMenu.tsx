"use client";

import { usePathname, useRouter } from "next/navigation";
import styles from "@/styles/SPMenu.module.scss";
import Link from "next/link";
import { tabs } from "./Tab";

export interface TabContent {
  title: string;
  url: string;
  isActive: boolean;
}

const SPMenu = ({ onClose }: { onClose: Function }) => {
  tabs.forEach((element) => {
    element.isActive = false;
  });

  const pathname = usePathname();
  const currentPath = "/" + pathname.split("/")[1];
  const currentTab = tabs.find((value) => value.url == currentPath);
  if (currentTab) currentTab.isActive = true;

  return (
    <div className={styles.root}>
      <div
        className={styles.background_div}
        onClick={(e) => {
          onClose();
          e.stopPropagation();
        }}
        onTouchMove={(e) => {
          onClose();
          e.stopPropagation();
        }}
      />
      <div className={styles.list}>
        <p className={styles.menu_title}>Menu</p>

        {tabs.map((tabContent, index) => {
          if (tabContent.title === "confidentials") {
            return (
              <div className={styles.reveal_container}>
                <div className={styles.overlay} />
                <Link className={styles.confidentials}
                  href={tabContent.url}
                  onClick={() => {
                    onClose();
                  }}
                  key={index}
                  prefetch={false}
                >
                  {tabContent.title}
                </Link>
              </div>
            );
          } else {
            return (
              <Link
                className={
                  tabContent.isActive ? styles.link_active : styles.link
                }
                href={tabContent.url}
                onClick={() => {
                  onClose();
                }}
                key={index}
                prefetch={false}
              >
                {tabContent.title}
              </Link>
            );
          }
        })}

        <div
          style={{
            display: "flex",
            flexDirection: "column",
            gap: 20,
            marginTop: 40,
          }}
        >
          <Link
            className={styles.ext_link}
            href="https://twitter.com/alphendp"
            target={"_blank"}
          >
            twitter
          </Link>
          <Link
            className={styles.ext_link}
            href="https://zatrec.bandcamp.com/music"
            target={"_blank"}
          >
            bandcamp
          </Link>
          <Link
            className={styles.ext_link}
            href="https://soundcloud.com/alph-endp"
            target={"_blank"}
          >
            soundcloud
          </Link>
          <Link
            className={styles.ext_link}
            href="https://gitlab.com/Innsbluck/zatrec"
            target={"_blank"}
          >
            gitlab
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SPMenu;
