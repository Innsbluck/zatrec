"use client";

import styles from "@/styles/Footer.module.scss";
import React from "react";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.container}>
        <img className={styles.motifs} src="/motifs.png" alt="" />
        <div className={styles.spacer}/>

        <p className={styles.copyrights}>
          ©2025 zattō records / All Rights Reserved.
        </p>
      </div>
    </footer>
  );
};

//Copyright ©2023 zattō records. All Rights Reserved.

export default React.memo(Footer);
